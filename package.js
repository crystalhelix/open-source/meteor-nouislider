Package.describe({
  name: 'crystalhelix:nouislider',
  version: '1.0.1',
  summary: 'post jQuery removal noUiSlider repackaged for Meteor',
  git: 'https://github.com/crystalhelix/meteor-nouislider',
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.1');
  api.use('ecmascript');
  api.addFiles([
    'src/distribute/nouislider.min.css',
    'src/distribute/nouislider.min.js'
  ], 'client');
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('crystalhelix:nouislider');
  api.addFiles(['nouislider-tests.js'], 'client');
});
