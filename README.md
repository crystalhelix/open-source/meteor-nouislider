# NoUiSlider for Meteor [![Build Status](https://travis-ci.org/crystalhelix/meteor-nouislider.svg)](https://travis-ci.org/crystalhelix/meteor-nouislider)

## Usage
This package makes the `noUiSlider` global variable available
on the client by default. For documentation, see the excellent examples [here](http://refreshless.com/nouislider/) or go directly to the noUiSlider repo [here](https://github.com/leongersen/noUiSlider/).

## Credits
All credits for this excellent javascript library go to the noUiSlider authors. This package is simply a wrapper to give you access to their awesome code.
